import React,{Fragment} from 'react';
import HomeContent from '../../components/HomeContent';

class Home extends React.Component{
    render(){
        return (
            <Fragment>
                <HomeContent/>
            </Fragment>
        )
    }
}
export default Home;