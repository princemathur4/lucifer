export const filters = [
    {
        "sizes": ['M','S','XS'],
        "gender": "male",
        "price": true,
         
        // "extra_filters": [{}]
    }
]

export const products = {
    "data": [
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
        {
            "product_id": "5dbc13a31c9d4400007aec0f",
            "category": "bottomwear",
            "sub_category": "jeans",
            "color": "black",
            "title": "Slim Fit Blue Denim Jeans",
            "image": ["https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9HVrD9DyMffpCjijjMi8UEANELfqo6u8_3NaQPCB_uEU6vGOS"],
            "sizes": [
                {
                    "size": "40",
                    "available_count": 20
                },
                {
                    "size": "42",
                    "available_count": 10
                }
            ],
            "gender": "male",
            "meta": {
                "material": "cotton"
            },
            "price": 1000
        },
    ],
        "success": true
}